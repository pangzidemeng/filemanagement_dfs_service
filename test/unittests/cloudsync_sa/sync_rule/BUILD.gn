# Copyright (C) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/test.gni")
import("//foundation/filemanagement/dfs_service/distributedfile.gni")

ohos_unittest("battery_status_listener_test") {
  branch_protector_ret = "pac_ret"
  sanitize = {
    ubsan = true
    boundary_sanitize = true
    cfi = true
    cfi_cross_dso = true
    debug = true
    blocklist = "${distributedfile_path}/cfi_blocklist.txt"
  }
  module_out_path = "filemanagement/dfs_service"
  sources = [
    "${distributedfile_path}/services/cloudsyncservice/src/sync_rule/battery_status_listener.cpp",
    "battery_status_listener_test.cpp",
  ]

  include_dirs = [
    "${services_path}/cloudsyncservice/include",
    "${services_path}/cloudsyncservice/include/sync_rule",
    "${innerkits_native_path}/cloudsync_kit_inner",
    "${distributedfile_path}/adapter/cloud_adapter_example/include",
    "${distributedfile_path}/test/unittests/cloudsync_sa/mock/data_syncer_rdb_store_mock.cpp",
    "${services_path}/cloudsyncservice/include/data_sync",
    "${utils_path}/log/include",
    "${image_framework_path}/mock/native/include/log",
    "${media_library_path}/interfaces/inner_api/media_library_helper/include",
    "${media_library_path}/frameworks/utils/include",
  ]

  deps = [
    "${services_path}/cloudsyncservice:cloudsync_sa_static",
    "${utils_path}:libdistributedfileutils",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "common_event_service:cesfwk_innerkits",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "init:libbegetutil",
    "ipc:ipc_core",
    "netmanager_base:net_conn_manager_if",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = [ "private=public" ]

  use_exceptions = true
}

ohos_unittest("net_conn_callback_observer_test") {
  module_out_path = "filemanagement/dfs_service"
  sources = [
    "${distributedfile_path}/services/cloudsyncservice/src/sync_rule/net_conn_callback_observer.cpp",
    "net_conn_callback_observer_test.cpp",
  ]

  include_dirs = [
    "${services_path}/cloudsyncservice/include",
    "${services_path}/cloudsyncservice/include/sync_rule",
    "${utils_path}/log/include",
    "${image_framework_path}/mock/native/include/log",
    "${innerkits_native_path}/cloudsync_kit_inner",
    "${distributedfile_path}/adapter/cloud_adapter_example/include",
    "${services_path}/cloudsyncservice/include/data_sync",
  ]

  deps = [
    "${services_path}/cloudsyncservice:cloudsync_sa_static",
    "${utils_path}:libdistributedfiledentry",
    "${utils_path}:libdistributedfileutils",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "common_event_service:cesfwk_innerkits",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "init:libbegetutil",
    "ipc:ipc_core",
    "netmanager_base:net_conn_manager_if",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = [ "private=public" ]

  use_exceptions = true
}

ohos_unittest("network_status_test") {
  module_out_path = "filemanagement/dfs_service"
  sources = [
    "${distributedfile_path}/services/cloudsyncservice/src/sync_rule/network_status.cpp",
    "network_status_test.cpp",
  ]

  include_dirs = [
    "${services_path}/cloudsyncservice/include",
    "${services_path}/cloudsyncservice/include/sync_rule",
    "${services_path}/cloudsyncservice/include/data_sync",
    "${utils_path}/log/include",
    "${image_framework_path}/mock/native/include/log",
    "${innerkits_native_path}/cloudsync_kit_inner",
    "${distributedfile_path}/adapter/cloud_adapter_example/include",
    "${services_path}/cloudsyncservice/include/data_sync",
    "${media_library_path}/interfaces/inner_api/media_library_helper/include",
    "${media_library_path}/frameworks/utils/include",
  ]

  deps = [
    "${services_path}/cloudsyncservice:cloudsync_sa_static",
    "${utils_path}:libdistributedfileutils",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "common_event_service:cesfwk_innerkits",
    "dfs_service:cloudsync_kit_inner",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "init:libbegetutil",
    "ipc:ipc_core",
    "netmanager_base:net_conn_manager_if",
    "relational_store:native_rdb",
    "samgr:samgr_proxy",
  ]

  defines = [ "private=public" ]

  use_exceptions = true
}

ohos_unittest("cloud_status_test") {
  module_out_path = "filemanagement/dfs_service"
  sources = [
    "${distributedfile_path}/adapter/cloud_adapter_example/src/dk_container.cpp",
    "${distributedfile_path}/adapter/cloud_adapter_example/src/dk_database.cpp",
    "${distributedfile_path}/adapter/cloud_adapter_example/src/dk_error.cpp",
    "${distributedfile_path}/adapter/cloud_adapter_example/src/dk_record_field.cpp",
    "${distributedfile_path}/adapter/cloud_adapter_example/src/drive_kit.cpp",
    "${distributedfile_path}/utils/log/src/dfs_error.cpp",
    "${services_path}/cloudsyncservice/src/sync_rule/cloud_status.cpp",
    "cloud_status_test.cpp",
  ]

  include_dirs = [
    "${services_path}/cloudsyncservice/include",
    "${services_path}/cloudsyncservice/include/sync_rule",
    "${media_library_path}/frameworks/utils/include",
    "${distributedfile_path}/adapter/cloud_adapter_example/include",
  ]

  deps = [
    "${services_path}/cloudsyncservice:cloudsync_sa_static",
    "${utils_path}:libdistributedfileutils",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "hilog:libhilog",
    "hisysevent:libhisysevent",
  ]

  defines = [ "private=public" ]

  use_exceptions = true
}

group("cloudsync_sa_sync_rule_test") {
  testonly = true
  deps = [
    ":battery_status_listener_test",
    ":cloud_status_test",
    ":network_status_test",
  ]
}
